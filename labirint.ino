#include <LiquidCrystal.h>

#define N 100
//0 - perete
//1 - liber
//2 - om
int labirint[16][16];
int iesire_x, iesire_y;
int intrare_x, intrare_y;
int om_x, om_y;
LiquidCrystal lcd(8,9,4,5,6,7);
byte caracter[3][8] = {
  {B11111,
  B11111,
  B11111,
  B11111,
  B11111,
  B11111,
  B11111,
  B11111},
  
  {B00000,
  B00000,
  B00000,
  B00000,
  B00000,
  B00000,
  B00000,
  B00000},
  
  {B11111,
  B10001,
  B10001,
  B10001,
  B10001,
  B10001,
  B10001,
  B11111},
};

void setup() {
  Serial.begin(9600);
  lcd.createChar(0, caracter[0]);
  lcd.createChar(1, caracter[1]);
  lcd.createChar(2, caracter[2]);
  lcd.begin(16, 2);

  for(int i=0; i<16; ++i)
  {
    labirint[0][i] = 0;
    labirint[15][i] = 0;
    labirint[i][0] = 0;
    labirint[i][15] = 0;
  }
  
  for(int i=1; i<15; ++i)
  {
    for(int j=1; j<15; ++j)
    {
      labirint[i][j] = 5;
    }
  }

  randomSeed(analogRead(A1));

  int perete = random(1, 5);
  if(perete == 1)
  {
    iesire_y = 0;
    iesire_x = random(1, 15);
  }else
  if(perete == 2)
  {
    iesire_x = 0;
    iesire_y = random(1, 15);
  }else
  if(perete == 3)
  {
    iesire_y = 15;
    iesire_x = random(1, 15);
  }else
  if(perete == 4)
  {
    iesire_x = 15;
    iesire_y = random(1, 15);
  }

  labirint[iesire_x][iesire_y] = 1;

  char candidat[200][2];
  int nr = 1;
  if(iesire_x == 15)
  {
    candidat[0][0] = iesire_x-1;
    candidat[0][1] = iesire_y;
    labirint[iesire_x-1][iesire_y] = 1;
  }else
  if(iesire_x == 0)
  {
    candidat[0][0] = iesire_x+1;
    candidat[0][1] = iesire_y;
    labirint[iesire_x+1][iesire_y] = 1;
  }else
  if(iesire_y == 15)
  {
    candidat[0][0] = iesire_x;
    candidat[0][1] = iesire_y-1;
    labirint[iesire_x][iesire_y-1] = 1;
  }else
  if(iesire_y == 0)
  {
    candidat[0][0] = iesire_x;
    candidat[0][1] = iesire_y+1;
    labirint[iesire_x][iesire_y+1] = 1;
  }

  int val = N;
  while(val > 0)
  {
    int pos = random(0, nr);
    int x = candidat[pos][0];
    int y = candidat[pos][1];

    int posibilitati = 0;
    
    if(labirint[x-1][y] == 5)
    {
      ++posibilitati;
    }
    if(labirint[x+1][y] == 5)
    {
      ++posibilitati;
    }
    if(labirint[x][y-1] == 5)
    {
      ++posibilitati;
    }
    if(labirint[x][y+1] == 5)
    {
      ++posibilitati;
    }

    int liber_nou = random(0, posibilitati);
    int nr_libere = 0;
    int x_nou, y_nou;
    if(labirint[x][y+1] == 5)
    {
      ++nr_libere;
    }

    if(nr_libere-1 == liber_nou)
    {
      int i;
      for(i=y+1; labirint[x][i] == 5; ++i);
      int lungime_drum = random(1, i-y);
      for(int i=1; i<=lungime_drum; ++i)
      {
        labirint[x][y+i] = 1;
        candidat[nr][0] = x;
        candidat[nr][1] = y+i;
        ++nr;
        --val;
      }
      
      nr_libere = -50;
    }else
    if(labirint[x+1][y] == 5)
    {
      ++nr_libere;
    }

    if(nr_libere-1 == liber_nou)
    {
      int i;
      for(i=x+1; labirint[i][y] == 5; ++i);
      int lungime_drum = random(1, i-x);
      for(int i=1; i<=lungime_drum; ++i)
      {
        labirint[x+i][y] = 1;
        candidat[nr][0] = x+i;
        candidat[nr][1] = y;
        ++nr;
        --val;
      }
      
      nr_libere = -50;
    }else
    if(labirint[x][y-1] == 5)
    {
      ++nr_libere;
    }

    if(nr_libere-1 == liber_nou)
    {
      int i;
      for(i=y-1; labirint[x][i] == 5; --i);
      int lungime_drum = random(1, y-i);
      for(int i=1; i<=lungime_drum; ++i)
      {
        labirint[x][y-i] = 1;
        candidat[nr][0] = x;
        candidat[nr][1] = y-i;
        ++nr;
        --val;
      }
      
      nr_libere = -50;
    }else
    if(labirint[x-1][y] == 5)
    {
      ++nr_libere;
    }

    if(nr_libere-1 == liber_nou)
    {
      int i;
      for(i=x-1; labirint[i][y] == 5; --i);
      int lungime_drum = random(1, x-i);
      for(int i=1; i<=lungime_drum; ++i)
      {
        labirint[x-i][y] = 1;
        candidat[nr][0] = x-i;
        candidat[nr][1] = y;
        ++nr;
        --val;
      }
      
      nr_libere = -50;
    }
  }

  int pos = random(0, nr);
  om_x = intrare_x = candidat[pos][0];
  om_y = intrare_y = candidat[pos][1];
  labirint[intrare_x][intrare_y] = 2;
  
  for(int i=0; i<16; ++i)
  {
    for(int j=0; j<16; ++j)
    {
      if(labirint[i][j] == 5)
      {
        labirint[i][j] = 0;
      }
      Serial.print(labirint[i][j]);
      Serial.print(" ");
    }
    Serial.println();
  }

 for(int i=0; i<16; ++i){
    lcd.setCursor(i,1);
    lcd.write(byte(labirint[om_x][i]));
    lcd.setCursor(i,0);
    lcd.write(byte(labirint[om_x-1][i]));
  }
}

int ultim;
int curent_om = 1;
void afiseaza(int linia1, int linia2)
{
  for(int i=0; i<16; ++i){
    lcd.setCursor(i,0);
    lcd.write(byte(labirint[linia1][i]));
    lcd.setCursor(i,1);
    lcd.write(byte(labirint[linia2][i]));
  }
  for(int i=0; i<16; ++i)
  {
    for(int j=0; j<16; ++j)
    {
      if(labirint[i][j] == 5)
      {
        labirint[i][j] = 0;
      }
      Serial.print(labirint[i][j]);
      Serial.print(" ");
    }
    Serial.println();
  }
}

int f=0;
void loop() {
  if(om_x == iesire_x && om_y == iesire_y)
  {
    //castig
    if(f==0){
      lcd.clear();
      f=1;
    }
    lcd.setCursor(0,0);
    lcd.print("Sfarsit");
  }
  int curent = analogRead(A0);
  if(ultim == 407 && curent != 407)
  {
    //stanga
    if(labirint[om_x][om_y-1] == 1)
    {
      labirint[om_x][om_y] = 1;
      labirint[om_x][om_y-1] = 2;
      --om_y;
      if(curent_om == 0)
      {
        afiseaza(om_x, om_x+1);
      }else
      {
        afiseaza(om_x-1, om_x);
      }
    }
  }

  if(ultim == 255 && curent != 255)
  {
    //jos
    if(labirint[om_x+1][om_y] == 1)
    {
      labirint[om_x][om_y] = 1;
      labirint[om_x+1][om_y] = 2;
      ++om_x;
    }
    curent_om = 0;
    if(curent_om == 0)
    {
      afiseaza(om_x, om_x+1);
    }else
    {
      afiseaza(om_x-1, om_x);
    }
  }
  
  if(ultim == 0 && curent != 0)
  {
    //dreapta
    if(labirint[om_x][om_y+1] == 1)
    {
      labirint[om_x][om_y] = 1;
      labirint[om_x][om_y+1] = 2;
      ++om_y;
      if(curent_om == 0)
      {
        afiseaza(om_x, om_x+1);
      }else
      {
        afiseaza(om_x-1, om_x);
      }
    }
  }

  if(ultim == 99 && curent != 99)
  {
    //sus
    if(labirint[om_x-1][om_y] == 1)
    {
      labirint[om_x][om_y] = 1;
      labirint[om_x-1][om_y] = 2;
      --om_x;
    }
    curent_om = 1;
    if(curent_om == 0)
    {
      afiseaza(om_x, om_x+1);
    }else
    {
      afiseaza(om_x-1, om_x);
    }
  }
  
  ultim = curent;
}
